<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserRegisterResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Validator;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password'=>'required'
        ]);

        if($validator->fails()){
            return response()->json(['message' => 'Validation errors', 'errors' =>  $validator->errors(), 'status' => false], 422);
        }

        $save = User::create(['name'=>$request->name,'email'=>$request->email,'password'=>Hash::make($request->password)]);

        return new UserRegisterResource($save);
    }
    public function login(Request $request){

    }
    //
}
