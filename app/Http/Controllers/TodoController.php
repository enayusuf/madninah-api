<?php

namespace App\Http\Controllers;

use App\Http\Resources\TodoResource;
use App\Models\Todo;
use Illuminate\Http\Request;
use Validator;

class TodoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function index(Request $request){
        $datas = Todo::orderBy('id')->get();

        return TodoResource::collection($datas);
    }


    public function store(Request $request){

        $validator = Validator::make($request->all(), ['owner'=>'required','due_date'=>'required']);


        if($validator->fails()){
            return response()->json(['message' => 'Validation errors', 'errors' =>  $validator->errors(), 'status' => false], 422);
        }

        $save = Todo::create($request->all());

        return new TodoResource($save);
    }

    public function update(Request $request, $id){
        $validator = Validator::make($request->all(), ['owner'=>'required','due_date'=>'required']);


        if($validator->fails()){
            return response()->json(['message' => 'Validation errors', 'errors' =>  $validator->errors(), 'status' => false], 422);
        }

        $save = Todo::find($id)->fill($request->all())->save();

        $saved = Todo::find($id);

        return new TodoResource($saved);

    }

    public function delete(Request $request){

        $find = Todo::find($request->id);

        if($find){
            $find->delete();
            return response()->json(['message'=>'Data deleted'],200);
        }else{
            return response()->json(['message'=>'Data not found'],422);

        }

    }
}
